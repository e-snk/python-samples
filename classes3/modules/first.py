class First:
    def __init__(self, sInstance, p1="param1", p2="param2"):
        print("First is entering")
        self.sInstance = sInstance
        self.c1p1 = p1
        self.c1p2 = p2
        self.c1p3 = "C1P3"
        print(f"First class: p1 = {self.c1p1}")
        print(f"First class: p2 = {self.c1p2}")
        print("First exiting")

    def printparam(self):
        print(f"First class p2 = {self.c1p2}")

    def printprm(self):
        print(f"First class p1 = {self.c1p1}")

    def updateFirstParam(self, value):
        self.c1p1 = value
        print(f'First class p1 in update = {self.c1p1}')
        self.c1p1 = self.sInstance.c2p1
        print(f'First class p1 in update2 = {self.c1p1}')

