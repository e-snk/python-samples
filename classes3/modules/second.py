from modules.first import First

class Second(First):
    def __init__(self, p1="c2param1", p2="c2param2", p3="c2param3"):
        print("Second is entering")
        self.c2p1 = p1
        self.c2p2 = p2
        self.c2p3 = p3
        print(f"Second class: p1 = {self.c2p1}")
        print(f"Second class: p2 = {self.c2p2}")
        print(f"Second class: p3 = {self.c2p3}")
        print("Second is exiting")
        First.__init__(
            self,
            p1='c1p1fromSecond',
            sInstance = self
        )

    def printparam(self):
        print(f"Second class p3 = {self.c2p3}")
        self.c2p1 = 'c2NewParam1'
        self.updateFirstParam('updatep1')
        print(f"First class in second p3 = {self.c1p3}")
        print(f"First class in second p1 = {self.c1p1}")
        self.printprm()
