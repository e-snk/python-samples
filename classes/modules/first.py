class First:
    def __init__(self, p1="param1", p2="param2"):
        print("First is entering")
        self.c1p1 = p1
        self.c1p2 = p2
        self.c1p3 = "C1P3"
        print(f"First class: p1 = {self.c1p1}")
        print(f"First class: p2 = {self.c1p2}")
        print("First exiting")

    def printparam(self):
        print(f"First class p2 = {self.c1p2}")

    def printprm(self):
        print(f"First class p1 = {self.c1p1}")
