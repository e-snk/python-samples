import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument('--ip', nargs='+', action='extend', help='IP addresses')

args = parser.parse_args()

ips = args.ip
print(json.dumps(ips, indent=2))