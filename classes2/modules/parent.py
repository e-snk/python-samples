class Parent:
    def __init__(self, child: object = None):
        print("Parent is entering")
        self.child = child
        self.c2p1 = "C2P1 in Parent init"
        print(f"Parent class: p1 = {self.c2p1}")
        print("Parent exiting")

    @property
    def c2p1(self) -> str:
        return self.__c2p1

    @c2p1.setter
    def c2p1(self, value: str):
        self.__c2p1 = value

    def printprm(self):
        print(f"Parent class p1 = {self.c2p1}")

    def updateParentParam(self, value):
        self.c2p1 = value
        print(f'Parent class p1 in update = {self.c2p1}')
        if self.child is not None:
            print(f'Child class p1 in Parent class update = {self.child.c2p1}')
