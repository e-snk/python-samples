from modules.parent import Parent

class Child(Parent):
    def __init__(self):
        print("Child is entering")
        self.c2p1 = "C2P1 in Child init"
        print(f"Child class: c2p1 = {self.c2p1}")
        print("Child is inheriting Parent")
        self.f = Parent(
            child = self
        )
        self.g = Parent()
        print("Child is existing")

    @property
    def c2p1(self) -> str:
        return self.__c2p1

    @c2p1.setter
    def c2p1(self, value: str):
        self.__c2p1 = value

    def printparam(self):
        print(f"Child class p1 in printparam = {self.c2p1}")
        self.c2p1 = 'c2NewParam1'
        print(f"Child class p1 in printparam updated = {self.c2p1}")
        self.f.printprm()
        self.f.updateParentParam('updatep1')
        print(f"Parent class in Child p1 = {self.f.c2p1}")
        self.f.printprm()
        self.g.printprm()
        self.g.updateParentParam('updatep1')
        print(f"Parent class in Child p1 = {self.g.c2p1}")
        self.g.printprm()
