import json
import os
from time import time

class Decorator(object):
    def __init__(self, *args, cacheDir=None, cacheFn=None, seconds=None, helper=None):
        self.args = args
        self.cacheDir = cacheDir
        self.cacheFn = cacheFn
        self.seconds = seconds
        self.helper = helper

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            myclassInstance = args[0]
            if self.cacheDir is None:
                self.cacheDir = myclassInstance.cacheDir
            if self.cacheFn is None:
                self.cacheFn = myclassInstance.cacheFn
            if self.seconds is None:
                self.seconds = myclassInstance.seconds

            if not self.helper(myclassInstance, 'FirstKey'):
                print('In Decorator, key is not starting with F')

            os.makedirs(self.cacheDir, exist_ok=True)
            fname = os.path.join(self.cacheDir, self.cacheFn)

            cached = None
            now = time()
            if os.path.exists(fname):
                with open(fname, 'rb') as fp:
                    cachedData = json.load(fp)
                if now - cachedData['TimeStamp'] < self.seconds:
                    cached = cachedData['Data']
            if cached is None:
                cached = func(*args)
                cachedData = {
                    'TimeStamp': time(),
                    'Data': cached.copy()
                }
                with open(fname, 'w') as fp:
                    json.dump(cachedData, fp)
            return cached
        return wrapper


class MyClass(object):
    def __init__(self):
        self.cacheDir = '.cached'
        self.cacheFn = 'cache.dat'
        self.seconds = 60

    def myhelper(self, key2Check):
        print(f'Checking key: {key2Check}')
        if key2Check[0] == 'F':
            print('First letter is F')
            return True
        print('First letter does not F')
        return False

    @Decorator(cacheFn='newCacheFile.dat', helper=myhelper)
    def read_all_data(self, key2check=None):
        print('Reading all data')
        all = json.load(open('sample-data/decorator-data.json'))
        return all

def main():
    myc = MyClass()
    search = input('Enter key to search: ')
    allData = myc.read_all_data(key2check=search)
    data = [ info for info in allData['sample_data'] if info['key'] == search ]
    if len(data) > 0:
        print(f"Value found for key: {data[0]['value']}")
    else:
        print('Key not found')

if __name__ == '__main__':
    main()
